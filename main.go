package main

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"log"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"

	"cloud.google.com/go/storage"
)

// basically the equivalent of:
//   cat urls.txt | gsutil -m cp -I dest
// but with more flexibility on the local destination filename
//
// input is in the format:
//   <key> <url>
//   ...
// e.g.
//   1850239293 gs://gitlab-gprd-artifacts/dd/5a/REDACTED/2021_12_04/REDACTED/REDACTED/job.log
//
// this will create a file `dest/1850239293`.

type request struct {
	ctx                           context.Context
	client                        *storage.Client
	destDir, localFileKey, gcsURL string
	line                          int
}

func processRecord(ctx context.Context, client *storage.Client, destDir, localFileKey, gcsURL string, line int) error {
	u, err := url.Parse(gcsURL)
	if err != nil {
		return err
	}

	if u.Scheme != "gs" {
		return fmt.Errorf("invalid input, non-gcs url on line %v", line)
	}

	if u.Host == "" {
		return fmt.Errorf("invalid input, empty host on line %v", line)
	}

	if u.Path == "" {
		return fmt.Errorf("invalid input, empty path on line %v", line)
	}

	destFile := path.Join(destDir, localFileKey)
	if _, err := os.Stat(destFile); !os.IsNotExist(err) {
		// file already exists, assume it is fine
		return nil
	}

	r, err := client.Bucket(u.Host).Object(u.Path[1:]).NewReader(ctx)
	if err != nil {
		return err
	}
	defer r.Close()

	fullDestDir := filepath.Dir(destFile)
	if _, err := os.Stat(fullDestDir); os.IsNotExist(err) {
		err = os.MkdirAll(fullDestDir, 0777)
		if err != nil {
			return err
		}
	}

	out, err := os.Create(destFile)
	if err != nil {
		return err
	}
	defer out.Close()

	w := bufio.NewWriter(out)
	defer w.Flush()

	_, err = io.Copy(w, r)
	if err != nil {
		return err
	}

	return nil
}

func main() {
	ctx := context.Background()

	if len(os.Args) < 2 {
		fmt.Println("usage:")
		fmt.Println("  gcs-batch-copy dest-dir < input")
		os.Exit(1)
	}
	destDir := os.Args[1]

	client, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatal(err)
	}

	ch := make(chan request, 16)

	workers := runtime.NumCPU()
	if os.Getenv("WORKERS") != "" {
		workers, err = strconv.Atoi(os.Getenv("WORKERS"))
		if err != nil {
			log.Fatal(err)
		}
		if workers < 1 {
			log.Fatal("worker count must be at least 1")
		}
	}

	for i := 0; i < workers; i++ {
		go func() {
			for req := range ch {
				err := processRecord(req.ctx, req.client, req.destDir, req.localFileKey, req.gcsURL, req.line)

				if err != nil {
					log.Printf("failed to process record %v on line %v: %v", req.localFileKey, req.line, err)
				}

				fmt.Println(path.Join(req.destDir, req.localFileKey))
			}
		}()
	}

	scanner := bufio.NewScanner(os.Stdin)
	line := 0
	for scanner.Scan() {
		line += 1

		if scanner.Text() == "" {
			continue
		}

		parts := strings.Split(scanner.Text(), " ")
		if len(parts) < 2 {
			log.Fatalf("invalid input, fewer than two fields on line %v", line)
		}
		if len(parts) > 2 {
			log.Fatalf("invalid input, more than two fields on line %v", line)
		}
		localFileKey := parts[0]
		gcsURL := parts[1]

		ch <- request{
			ctx:          ctx,
			client:       client,
			destDir:      destDir,
			localFileKey: localFileKey,
			gcsURL:       gcsURL,
			line:         line,
		}
	}
	close(ch)

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	time.Sleep(1 * time.Second)
}
